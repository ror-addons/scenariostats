<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="ScenarioStats" version="1.4" date="12/02/2008">
	<Author name="Jarika" email="jarika@gmx.de" />
	<Description text="Displays Statistic over played scenarios. Use /sstats" />
        <VersionSettings gameVersion="1.3.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Files>
            <File name="ScenarioStats.lua" />
			<File name="ScenarioStats.xml" />
        </Files>
		

    </UiMod>
</ModuleFile>
