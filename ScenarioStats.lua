ScenarioStats = {}

local  VERSION = "1.4"

-- Debug variable, set to true for debugging messages to the /debug log. Prepare to be spammed, you have been warned.
local DEBUG = false

local DEFAULTVALUES = {		Order = 0,
							Destruction = 0,
							Tie = 0,
							HighestDmg = 0,
							HighestHeal = 0,
							HighestKillBlows = 0,
							HighestKills = 0,
							HighestDeaths = 0,
							HighestSoloKills = 0,
							HighestRenown = 0,
							HighestXP = 0,
							-- Data for average calculations
							AvgDmg = 0,
							AvgHeal = 0,
							AvgKillBlows = 0,
							AvgKills = 0,
							AvgDeaths = 0,
							AvgSoloKills = 0,
							AvgRenown = 0,
							AvgXP = 0,
							TotalGames = 0,
						}

local DEFAULT_CONFIG = {
							["Damage"] = true,
							["Heal"] = true,
							["Killing Blows"] = true,
							["Kills"] = true,
							["Deaths"] = true,
							["Solo Kills"] = true,
							["Renown"] = true,
							["Experience"] = true,
							version = 
									{
										highNumber = 1,
										minorNumber = 0,
									},
						}
												
local DEFAULT_ORDER = {
							[1] = "Damage",
							[2] = "Heal",
							[3] = "Killing Blows",
							[4] = "Kills",
							[5] = "Deaths",
							[6] = "Solo Kills",
							[7] = "Renown",
							[8] = "Experience",
						}
							
local ScenarioTiers = 	{ 	
							[2000] = "Tier 1",
							[2100] = "Tier 1",
							[2200] = "Tier 1",

							[2001] = "Tier 2",
							[2101] = "Tier 2",
							[2201] = "Tier 2",

							[2102] = "Tier 3",
							[2103] = "Tier 3",
							[2202] = "Tier 3",
							[2203] = "Tier 3",
							[2002] = "Tier 3",
							[2011] = "Tier 3",

							[2005] = "Tier 4",
							[2006] = "Tier 4",
							[2106] = "Tier 4",
							[2107] = "Tier 4",
							[2204] = "Tier 4",
							[2205] = "Tier 4",
-- Unlocked:
							[2004] = "Tier 4",
							[2008] = "Tier 4",
							[2104] = "Tier 4",
							[2109] = "Tier 4",
							[2206] = "Tier 4",
							[2207] = "Tier 4",
							[2108] = "Tier 4",
-- Unknown:
							[2003] = "Tier 4",
							[2007] = "Tier 4",
							[2009] = "Tier 4",
							[2010] = "Tier 4",
							[2012] = "Tier 4",
							[2013] = "Tier 4",
							[2105] = "Tier 4",
							[2111] = "Tier 4",

							}

local SCENARIO_ORDER = 	{
							2000, 2100, 2200, --tier 1
							2001, 2101, 2201, --tier 2
							2102, 2103,	2202, 2203, 2002, 2011, --tier 3
							2005, 2006, 2106, 2107, 2204, 2205, -- tier 4
							2004, 2008, 2104, 2109, 2206, 2207, -- Unlocked tier 4
							2108, 	--Reikland Factory
							2003, 2007, 2009, 2010, 2012, 2013, 2105, 2111, --unknown (City or disabled)
						}
											
											
local function Print(arg)
	EA_ChatWindow.Print(L"[Scenario Statistics]: "..towstring(arg))
end

local function deb(arg)
	if (DEBUG) then d(arg) end
end

local function round(num, idp)
  return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end

function comma_value(amount)
  local formatted = amount
  while true do  
    formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
    if (k==0) then
      break
    end
  end
  return formatted
end

local function deepcopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return setmetatable(new_table, getmetatable(object))
    end
    return _copy(object)
end
	
local function OptionalDependency(AddonName, secondTime)
	local mods = ModulesGetData()
	for k,v in ipairs(mods) do
		if v.name == AddonName then
			if not secondTime then -- only run this on the initial run
				if v.isEnabled and not v.isLoaded then
					d("Force loading addon: "..AddonName)
					ModuleInitialize(v.name)
				end
				if (v.isEnabled and v.isLoaded) or OptionalDependency(AddonName, true) then
					d("loaded: "..AddonName)
					return true
				else
					d("error loading: "..AddonName)
					return false -- it's installed but disabled, or there was an error loading it
				end
			end
			return (v.isEnabled and v.isLoaded) -- This part only gets executed on the internal second run
		end
	end
	d(AddonName.." not found")
	return false -- addon not installed
end

function ScenarioStats.Initialize()
-- Register event handlers and slash commands
	
	ScenarioStats.CheckSettingsInit()
	ScenarioStats.GetScenarioNames()
	if (GameData.Player.isInScenario) then
-- If player is in a scenario while addon loads (after /reloadui e.g), simulate SCENARIO_BEGIN event.
		deb("Player is already in a scenario. Reseting values")
		ScenarioStats.ScenarioStarts()
	end
	ScenarioStats.LoadWindow()
	deb("ScenarioStats loaded")
	d("ScenarioStats loaded")
end

function ScenarioStats.toggleAddon()
	WindowUtils.ToggleShowing("ScenarioStatsWindow")
end


function ScenarioStats.LoadWindow()
	CreateWindow("ScenarioStatsWindow", false)
	WindowSetShowing("ScenarioStatsWindow", false)
	
	--WindowSetDimensions("ScenarioStatsWindow", 300, 500)
	LabelSetText("ScenarioStatsWindowTitle", L"Scenario Stats")
--	LabelSetTextColor("ScenarioStatsWindowTitle", 254, 254, 254)
	LabelSetText("ScenarioStatsWindowOrder", L"Order")
	LabelSetText("ScenarioStatsWindowTie", L"Tie")
	LabelSetText("ScenarioStatsWindowDestruction", L"Destruction")
	LabelSetText("ScenarioStatsWindowColumn1Title", L"")
	LabelSetText("ScenarioStatsWindowColumn2Title", L"Top Score")
	LabelSetText("ScenarioStatsWindowColumn3Title", L"Average Score")

--[[	ComboBoxClearMenuItems("ScenarioStatsWindowScenarioNameCombo")
	ComboBoxAddMenuItem("ScenarioStatsWindowScenarioNameCombo", L"ALL Scenarios" )
	local text =""
	for k,v in ipairs(SCENARIO_ORDER) do
		deb(k.." - "..v)
		text = towstring(ScenarioTiers[v])..L" - "..towstring(ScenarioStats.ScenarioNames[v])
		ComboBoxAddMenuItem("ScenarioStatsWindowScenarioNameCombo", towstring(text) )
	end]]--
	deb("Calling Dropdownlist, Loadwindow")
	ScenarioStats.CreateDropdownList()
	deb("Window created")
end

function ScenarioStats.CreateDropdownList()
	ScenarioStats.DropdownList = {}
	ComboBoxClearMenuItems("ScenarioStatsWindowScenarioNameCombo")
	ComboBoxAddMenuItem("ScenarioStatsWindowScenarioNameCombo", L"ALL Scenarios" )
	table.insert(ScenarioStats.DropdownList, L"all")
	local text =""
	for k,v in ipairs(SCENARIO_ORDER) do
		deb(k.." - "..v)
		if (ScenarioStats.currentView.Data[v]) then --data exists for this scenario
			text = towstring(ScenarioTiers[v])..L" - "..towstring(ScenarioStats.ScenarioNames[v])
			ComboBoxAddMenuItem("ScenarioStatsWindowScenarioNameCombo", towstring(text) )
			table.insert(ScenarioStats.DropdownList, towstring(ScenarioStats.ScenarioNames[v]))
			deb(L"Added Sceanrio: "..ScenarioStats.ScenarioNames[v])
		else
			deb(L"Didn't add Scenario: "..ScenarioStats.ScenarioNames[v])
		end
	end
end

function ScenarioStats.UpdateWindow(DataSet)
	ComboBoxSetSelectedMenuItem("ScenarioStatsWindowScenarioNameCombo", 1)
	for i,k in ipairs(ScenarioStats.DropdownList) do
		if (ScenarioStats.DropdownList[i] == DataSet.name) then
			ComboBoxSetSelectedMenuItem("ScenarioStatsWindowScenarioNameCombo", i)
			ButtonSetText("ScenarioStatsWindowScenarioNameComboSelectedButton", towstring(DataSet.name))
		end
	end
	
	
	LabelSetText("ScenarioStatsWindowOrderPoints", L""..towstring(DataSet.order)..L" ("..towstring(DataSet.orderPercent)..L"%)")
	LabelSetText("ScenarioStatsWindowTiePoints", L""..towstring(DataSet.tie)..L" ("..towstring(DataSet.tiePercent)..L"%)")
	LabelSetText("ScenarioStatsWindowDestructionPoints", L""..towstring(DataSet.destruction)..L" ("..towstring(DataSet.destructionPercent)..L"%)")
	local i = 0

	for idx,name in ipairs(DEFAULT_ORDER) do
		deb(idx.." - "..name.." - "..tostring(ScenarioStats.Config[name]))
		if (ScenarioStats.Config[name]) then 
			i = i+1
			LabelSetText("ScenarioStatsWindowStat"..i.."Name", towstring(name))
			LabelSetText("ScenarioStatsWindowStat"..i.."TopScore", towstring(comma_value(DataSet.Highest[name])))
			LabelSetText("ScenarioStatsWindowStat"..i.."Average", towstring(comma_value(DataSet.Average[name])))
		--Print(L"Highest Damage: "..set.HighestDmg..L"   -   Average Damage: "..set.AvgDmg) 
		end
	end
	k = i + 1
	for e = k,8 do
-- make sure unused labels are empty	
		LabelSetText("ScenarioStatsWindowStat"..e.."Name", L"")
		LabelSetText("ScenarioStatsWindowStat"..e.."TopScore", L"")
		LabelSetText("ScenarioStatsWindowStat"..e.."Average", L"")
	end
	local height = 250 + i * 25
	WindowSetDimensions("ScenarioStatsWindow", 500, height)
	ButtonSetText("ScenarioStatsWindowConfigButton", L"Config")
	WindowSetScale("ScenarioStatsWindowConfigButton", 0.65)
	WindowSetDimensions("ScenarioStatsWindowConfigButton", 110, 39)
	WindowSetShowing("ScenarioStatsWindow", true)
	ComboBoxClearMenuItems("ScenarioStatsWindowCharacterNameCombo")
	local index = 0
	for id,character in ipairs(ScenarioStats.CharacterList) do
		text = towstring(character.Name)..L" / "..towstring(character.Server)
		ComboBoxAddMenuItem("ScenarioStatsWindowCharacterNameCombo", towstring(text) )
		index = index +1
		if (character.Server == ScenarioStats.currentView.Server) and (character.Name == ScenarioStats.currentView.Name) then 
			deb(L"Found match "..text..L" at index "..index)
			ComboBoxSetSelectedMenuItem("ScenarioStatsWindowCharacterNameCombo", index)
		end
	end
	
end

function ScenarioStats.WindowClose()
	deb("Closing Window")
	WindowSetShowing("ScenarioStatsWindow", false)
	ScenarioStats.SettingsWindowCancel()
end

function ScenarioStats.ScenarioSelectionChanged()
	selection = ComboBoxGetSelectedMenuItem("ScenarioStatsWindowScenarioNameCombo")
	deb(selection)
	if (selection == 0) then
		deb("Selection changed to: "..selection.." - ALL Scenarios")
		ScenarioStats.ShowWindowByName("all")
	else
		deb(L"Selection changed to: "..towstring(selection)..L" - "..towstring(ScenarioStats.DropdownList[selection]))
		ScenarioStats.ShowWindowByName(WStringToString(ScenarioStats.DropdownList[selection]))
	end
	ScenarioStats.Config.LastViewed = selection
end

function ScenarioStats.CharacterSelectionChanged()
	local selection = ComboBoxGetSelectedMenuItem("ScenarioStatsWindowCharacterNameCombo")
	if selection == 0 then return end -- cleared list
	local server = ScenarioStats.CharacterList[selection].Server
	local character = ScenarioStats.CharacterList[selection].Name
	ScenarioStats.currentView.Data = ScenarioStats.Data[server][character]
	ScenarioStats.currentView.Name = character
	ScenarioStats.currentView.Server = server
	deb("Calling Dropdownlist, SelectionChanged")
	ScenarioStats.CreateDropdownList()
	ScenarioStats.ScenarioSelectionChanged()
end

function ScenarioStats.CallSettingsWindow()
	deb(L"Call Settings")
	if not (DoesWindowExist("ScenarioConfigWindow")) then 
		CreateWindow("ScenarioConfigWindow", true)
	end
	WindowClearAnchors("ScenarioConfigWindow")
	WindowAddAnchor("ScenarioConfigWindow", "top", "ScenarioStatsWindow", "top", 0, 30)
	WindowSetShowing("ScenarioConfigWindow", true)
	LabelSetText("ScenarioConfigWindowTitle", L"Settings")
	local i = 0
	for idx,name in ipairs(DEFAULT_ORDER) do
		i = i+1
		LabelSetText("ScenarioConfigWindowLabel"..i, towstring(name))
		ButtonSetCheckButtonFlag("ScenarioConfigWindowCheckBox"..i, true)
		ButtonSetPressedFlag("ScenarioConfigWindowCheckBox"..i, ScenarioStats.Config[name])
	end
	ButtonSetText("ScenarioConfigWindowSave", L"Save")
end

function ScenarioStats.SettingsWindowClose()
	WindowSetShowing("ScenarioConfigWindow", false)
	for idx,name in ipairs(DEFAULT_ORDER) do
		ScenarioStats.Config[name] = ButtonGetPressedFlag("ScenarioConfigWindowCheckBox"..idx)
		deb("Setting "..(name).." to "..tostring(ScenarioStats.Config[name]))
	end
	WindowSetScale("ScenarioConfigWindowSave", 0.7)
	ScenarioStats.ScenarioSelectionChanged()
end

function ScenarioStats.SettingsWindowCancel()
	WindowSetShowing("ScenarioConfigWindow", false)
end

function ScenarioStats.GetScenarioNames()
	deb("Trying to get scenario names")
	ScenarioStats.ScenarioNames = {}
	ScenarioStats.ScenarioID = {}
	for i = 2000,2300 do
		local value = ScenarioStats.GetScenName(i)
		if value then
			ScenarioStats.ScenarioNames[i] = value
			ScenarioStats.ScenarioID[value] = i
		end
	end
	deb("Got all Scenario Names:")
	if DEBUG then
		for i,k in pairs(ScenarioStats.ScenarioNames) do
			deb(i..L" - "..k)
		end
	end
end

function ScenarioStats.CheckSettings(id, settings)
	if not id then
		deb("Check Settings for Root")
		check = settings
		id = "Root"
	else
		deb("Check Settings for ID "..id)
		check = settings[id]
	end
	if not check then
		deb("ID "..id.." does not exist yet, creating with default values")
		check = deepcopy(DEFAULTVALUES)
		return -- can safely return here, since it will have all the default settings now
	end
	for name,value in pairs(DEFAULTVALUES) do
		if not check[name] then
			deb("ID: "..id.." - Name: "..name.." - Value: "..value.." does NOT exist, creating")
			check[name] = value
		end
	end
end

function ScenarioStats.CheckSettingsInit()
-- check if required variables exist, if not initialize them with default values
	deb("Initial Settings Check started")
	if (ScenarioStatsSettings ~= nil) then -- version 0.2 or earlier. Converting to 0.3 format
		d("Old Settings found, converting")
		if not (ScenarioStats.Data) then
			ScenarioStats.Data = ScenarioStatsSettings
			ScenarioStatsSettings = nil
			d("Convertion complete")
		else
			d("New data found already, doing nothing")
			ScenarioStatsSettings = nil
		end
	end
	if not (ScenarioStats.Data) then -- completely empty
		ScenarioStats.Data = 	{   -- creating empty 1.0 set
						version =	{
							highNumber = 1,
							minorNumber = 0,
									}
								} 
	end
	if not (ScenarioStats.Data.version) then -- lower than 1.0
		ScenarioStats.Warning03()
		return
	end

	--checking if dataset for current character exists, if not create it
	local server = WStringToString(SystemData.Server.Name)
	local character = WStringToString(GameData.Player.name)
	if not (ScenarioStats.Data[server]) then -- current server does not exist
		deb("Creating entry for Server: "..server)
		ScenarioStats.Data[server] = {}
	end
	if not (ScenarioStats.Data[server][character]) then -- current player does not exist
		deb("Creating default settings for Character "..character.."/"..server)
		ScenarioStats.Data[server][character] = deepcopy(DEFAULTVALUES)
	end
	
	ScenarioStats.CharacterList = {}
	-- validating all data
	for id,servertable in pairs(ScenarioStats.Data) do
		if (id == "version") then
		-- do stuff to version
		else -- its a Servername
			for id2,character in pairs(servertable) do
				local toCheckSet = character
				deb("Now checking :"..id.."/"..id2)
				ScenarioStats.CheckSettings(nil, toCheckSet)
				table.insert(ScenarioStats.CharacterList, {Server = id , Name = id2} )
				for id3, scenarioID in pairs(toCheckSet) do
					if (tonumber(id3)) then  -- is it actually a scenario or an ALL Scenario Data?
						ScenarioStats.CheckSettings(id3, toCheckSet)
					end --if
				end -- for
			end --for
		end --ifelse
	end --for
	
	if not ScenarioStats.Config then
		ScenarioStats.Config = deepcopy(DEFAULT_CONFIG)
	end
	if not (ScenarioStats.Config.version) then
		ScenarioStats.Config.version = {
							highNumber = 1,
							minorNumber = 0,
									}
	end
	
	ScenarioStats.thisCharacterData = ScenarioStats.Data[server][character]
	ScenarioStats.currentView = {}
	ScenarioStats.currentView.Data = ScenarioStats.thisCharacterData
	ScenarioStats.currentView.Server = server
	ScenarioStats.currentView.Name = character
	
	RegisterEventHandler("Root", SystemData.Events.SCENARIO_END, "ScenarioStats.UpdateStats")
    RegisterEventHandler("Root", SystemData.Events.SCENARIO_BEGIN, "ScenarioStats.ScenarioStarts")
    RegisterEventHandler("Root", SystemData.Events.SCENARIO_UPDATE_POINTS, "ScenarioStats.UpdatePoints")
	if OptionalDependency("LibSlash") then LibSlash.RegisterSlashCmd("sstats", function(input) ScenarioStats.Slash(input) end) 
	else
		deb("Libslash not loaded")
	end
end

function ScenarioStats.ScenarioStarts()
-- scenario starts, set everything to 0, just to make sure
	ScenarioStats.orderPoints = 0
	ScenarioStats.destructionPoints = 0
	ScenarioStats.scenarioID = GameData.ScenarioData.id
	deb("Scenario started, current points to 0")
end

function ScenarioStats.UpdatePoints()
-- Keep track of current points
	ScenarioStats.orderPoints = GameData.ScenarioData.orderPoints
	ScenarioStats.destructionPoints = GameData.ScenarioData.destructionPoints
end

function ScenarioStats.GetScenarioData()
	local DataSet = {}
	if (ScenarioSummaryWindowPlayerList.PopulatorIndices) then
		for i,k in ipairs (ScenarioSummaryWindowPlayerList.PopulatorIndices) do
			local playername = ScenarioSummaryWindow.playersData[k].name
			if playername == GameData.Player.name then
					    local data = ScenarioSummaryWindow.playersData[k]
						deb("End of Scenario: Found player, saved list")
						DataSet.Damage = data.damagedealt
						DataSet.Heal = data.healingdealt
						DataSet.KB = data.deathblows
						DataSet.Kills = data.groupkills
						DataSet.Deaths = data.deaths
						DataSet.SoloKills = data.solokills
						DataSet.Renown = data.renown
						DataSet.XP = data.experience
						return DataSet
			end
		end
	end
end

function ScenarioStats.UpdateStats()
-- At end of scenario, calculate winner and give points.
	local orderPoints = ScenarioStats.orderPoints
	local destPoints = ScenarioStats.destructionPoints
	local id = ScenarioStats.scenarioID
	if ( id == 0 ) or ( id == nil ) then
		d("CRITICAL ERROR: ID is nil or 0 - should never happen")
		return
	end
	if (orderPoints > destPoints) then
		ScenarioStats.addWin("order", id, 1)
		deb("Order won - Order:"..orderPoints.." - Destruction: "..destPoints)
	end
	if (orderPoints < destPoints) then
		ScenarioStats.addWin("dest", id, 1)
		deb("Destruction won - Order:"..orderPoints.." - Destruction: "..destPoints)
	end
	if (orderPoints == destPoints) then
		ScenarioStats.addWin("tie", id, 1)
		deb("Tie - Order:"..orderPoints.." - Destruction: "..destPoints)
	end
	local DataSet = ScenarioStats.GetScenarioData()
	ScenarioStats.addStats(DataSet, id, ScenarioStats.thisCharacterData)
	ScenarioStats.orderPoints = 0
	ScenarioStats.destructionPoints = 0
	ScenarioStats.scenarioID = 0
end

function ScenarioStats.ShowCurrent()
-- Shows points of currently running scenario. 0 if outside. Used for debugging
	Print(L"Current Order Points: "..GameData.ScenarioData.orderPoints)
	Print(L"Current Destruction Points: "..GameData.ScenarioData.destructionPoints)
end


function ScenarioStats.ShowWindowByName(name)
	local DataSet
	local server = WStringToString(SystemData.Server.Name)
	local character = WStringToString(GameData.Player.name)
	if towstring(name) == L"" then
		deb(L"No name specified, showing last scenario")
		if not ScenarioStats.Config.LastViewed then ScenarioStats.Config.LastViewed = 0 end
		if not ScenarioStats.DropdownList[ScenarioStats.Config.LastViewed] then ScenarioStats.Config.LastViewed = 0 end
		ScenarioStats.currentView.Data = ScenarioStats.thisCharacterData
		ScenarioStats.currentView.Server = server
		ScenarioStats.currentView.Name = character
		if (ScenarioStats.Config.LastViewed == 0) then
			ScenarioStats.ShowWindowByName("all")
		else
			ScenarioStats.ShowWindowByName(ScenarioStats.DropdownList[ScenarioStats.Config.LastViewed])
		end
		return
	end
	if towstring(name) == L"all" then
		deb(L"No name specified, showing all scenario")
		DataSet = ScenarioStats.GenerateDataSet()
		ScenarioStats.UpdateWindow(DataSet)
		return
	end
	id = ScenarioStats.ScenarioID[towstring(name)]
	if id then 
		DataSet = ScenarioStats.GenerateDataSet(id)
		ScenarioStats.UpdateWindow(DataSet)
	else 
		Print(L"Sorry, but something went wrong. Please inform the author ( Jarika@gmx.de ) of this problem: Scenario \""..towstring(name)..L"\" could not be found")
	end
end

function ScenarioStats.PrintStatsByName(name)
	if name == "" then
		deb(L"No name specified, showing all scenarios")
		ScenarioStats.PrintStats()
		return
	end
--[[	for id,scenario in pairs(ScenarioStats.ScenarioNames) do
		deb(L"Comparing "..towstring(name)..L" to "..scenario)
		if (WStringsCompare(wstring.upper(scenario) , wstring.upper(towstring(name))) == 0) then -- if supplied name matches one found in table
			deb(L"Found match for Scenario \""..towstring(name)..L"\" ID: "..id)
			ScenarioStats.PrintStats(id)
			return
		end
	end 
-- if we arrive here, no match has been found. ]]--
	id = ScenarioStats.ScenarioID[towstring(name)]
	if id then ScenarioStats.PrintStats(id)
	else Print(L"Sorry, but no Scenario by the name of \""..towstring(name)..L"\" could be found. Please check your spelling, or type /sstats showNames for a list of valid Scenario names")
	end
end

function ScenarioStats.PrintStats(id)
-- Outputs the stats to the Chatwindow
	local order
	local dest
	local tie
	local name
	if not id then
		deb("No ID specified ... showing statistics for all scenarios")
		order = ScenarioStats.thisCharacterData.Order
		dest = ScenarioStats.thisCharacterData.Destruction
		tie = ScenarioStats.thisCharacterData.Tie
		name = L"All Scenarios"
		set = ScenarioStats.thisCharacterData
	elseif not (ScenarioStats.thisCharacterData[id]) then
-- No Stats available for requested scenario
		deb("No Stats available for requested scenario ID "..id)
		order = 0
		dest = 0
		tie = 0
		name = ScenarioStats.ScenarioNames[id]
		set = DEFAULTVALUES
	else
-- valid id given and it exists
		order = ScenarioStats.thisCharacterData[id].Order
		dest = ScenarioStats.thisCharacterData[id].Destruction
		tie = ScenarioStats.thisCharacterData[id].Tie
		name = ScenarioStats.ScenarioNames[id]
		set = ScenarioStats.thisCharacterData[id]
		if not (name) then 
-- invalid scenario name, yet data exists ... odd
			d("CRITICAL ERROR: Invalid Name for a valid ID! Terminating")
			return 
		end
	end
	local total = order + dest + tie
	local orderPercent = 0
	local destPercent = 0
	local tiePercent = 0
	if (total > 0) then
		orderPercent = round(order / total * 100, 1)
		destPercent = round(dest / total * 100, 1 )
		tiePercent = round(tie / total * 100,1)
	end
	Print(L"Summery for Scenario: "..name)
	Print(L"--------------------")
	Print(L"Order wins: "..order..L" ("..orderPercent..L"%)")
	Print(L"Destruction wins: "..dest..L" ("..destPercent..L"%)")
	Print(L"Tied: "..tie..L" ("..tiePercent..L"%)")
	if (DEBUG) then -- for now
	if (ScenarioStats.Config.Damage) then Print(L"Highest Damage: "..set.HighestDmg..L"   -   Average Damage: "..set.AvgDmg) end
	if (ScenarioStats.Config.Heal) then Print(L"Highest Heal: "..set.HighestHeal..L"   -   Average Heal: "..set.AvgHeal) end
	if (ScenarioStats.Config.KB) then Print(L"Highest Killing Blows: "..set.HighestKillBlows..L"   -   Average Killing Blows: "..set.AvgKillBlows) end
	if (ScenarioStats.Config.SK) then Print(L"Highest Solo Kills: "..set.HighestSoloKills..L"   -   Average Solo Kills: "..set.AvgSoloKills) end
	if (ScenarioStats.Config.Kills) then Print(L"Highest Kills: "..set.HighestKills..L"   -   Average Kills: "..set.AvgKills) end
	if (ScenarioStats.Config.Deaths) then Print(L"Highest Deaths: "..set.HighestDeaths..L"   -   Average Deaths: "..set.AvgDeaths) end
	if (ScenarioStats.Config.Renown) then Print(L"Highest Renown: "..set.HighestRenown..L"   -   Average Renown: "..set.AvgRenown) end
	if (ScenarioStats.Config.XP) then Print(L"Highest XP: "..set.HighestXP..L"   -   Average XP: "..set.AvgXP) end
	end
end

function ScenarioStats.GenerateDataSet(id)
-- Generates DataSet for output via UpdateWindow
	local name
	local set
	if not id then
		deb("No ID specified ... showing statistics for all scenarios")
		name = L"All Scenarios"
		set = ScenarioStats.currentView.Data
	elseif not (ScenarioStats.currentView.Data[id]) then
-- No Stats available for requested scenario
		deb("No Stats available for requested scenario ID "..id)
		name = ScenarioStats.ScenarioNames[id]
		set = DEFAULTVALUES
	else
-- valid id given and it exists
		name = ScenarioStats.ScenarioNames[id]
		set = ScenarioStats.currentView.Data[id]
		if not (name) then 
-- invalid scenario name, yet data exists ... odd
			d("CRITICAL ERROR: Invalid Name for a valid ID! Terminating")
			return false
		end
	end
	local order = set.Order
	local dest = set.Destruction
	local tie = set.Tie
	
	local total = order + dest + tie
	local orderPercent = 0
	local destPercent = 0
	local tiePercent = 0
	if (total > 0) then
		orderPercent = round(order / total * 100, 1)
		destPercent = round(dest / total * 100, 1 )
		tiePercent = round(tie / total * 100,1)
	end
-- Generating DataSet
	local DataSet = {}
	DataSet.name = name
	DataSet.order = order
	DataSet.destruction = dest
	DataSet.tie = tie
    DataSet.orderPercent = orderPercent
	DataSet.destructionPercent = destPercent
	DataSet.tiePercent = tiePercent
	DataSet.Highest={}
	DataSet.Average={}
	DataSet.Highest["Damage"] = set.HighestDmg
	DataSet.Highest["Heal"] = set.HighestHeal
	DataSet.Highest["Killing Blows"] = set.HighestKillBlows
	DataSet.Highest["Solo Kills"] = set.HighestSoloKills
	DataSet.Highest["Kills"] = set.HighestKills
	DataSet.Highest["Deaths"] = set.HighestDeaths
	DataSet.Highest["Renown"] = set.HighestRenown
	DataSet.Highest["Experience"] = set.HighestXP
	DataSet.Average["Damage"] = set.AvgDmg
	DataSet.Average["Heal"] = set.AvgHeal
	DataSet.Average["Killing Blows"] = set.AvgKillBlows
	DataSet.Average["Solo Kills"] = set.AvgSoloKills
	DataSet.Average["Kills"] = set.AvgKills
	DataSet.Average["Deaths"] = set.AvgDeaths
	DataSet.Average["Renown"] = set.AvgRenown
	DataSet.Average["Experience"] = set.AvgXP
	return DataSet
end

local function Reset(confirm)
-- Will delete all previous stats without warning!!! Be very very careful with this.
	if confirm==L"AlphaDeltaWipe" then
		Print(L"Wiping all data...")
		ScenarioStats.Data = nil
		ScenarioStats.CheckSettingsInit()
		Print(L"Data Wipe complete")
	end
end

function ScenarioStats.checkHigher(Settings, DataSet, id, topScoreName, DataSetName)
-- check overall top Score
	if (Settings[topScoreName] < DataSet[DataSetName]) then
-- new topscore
		deb("New total Topscore: "..topScoreName.." - "..DataSet[DataSetName].." - old: "..Settings[topScoreName])
		Settings[topScoreName] = DataSet[DataSetName]
	end
	local ScenarioSettings = Settings[id]
	if (ScenarioSettings[topScoreName] < DataSet[DataSetName]) then
-- new topscore
		deb("New Scenario Topscore: "..topScoreName.." - "..DataSet[DataSetName].." - old: "..ScenarioSettings[topScoreName])
		ScenarioSettings[topScoreName] = DataSet[DataSetName]
	end
end

function ScenarioStats.addAverage(Settings, DataSet, id, avgScoreName, DataSetName)
	set = Settings
	local total = set[avgScoreName] * set.TotalGames
	local newTotal = total + DataSet[DataSetName]
	local newScore = round( newTotal / (set.TotalGames + 1) , 0 )
	set[avgScoreName] = newScore
	
	set = Settings[id]
	local total = set[avgScoreName] * set.TotalGames
	local newTotal = total + DataSet[DataSetName]
	local newScore = round( newTotal / (set.TotalGames + 1) , 0 )
	set[avgScoreName] = newScore
	
end


function ScenarioStats.addStats(DataSet, id, Settings)
	if not id or not DataSet or not Settings then
		d("Critical Error: addWin function called with incorrect Parameters")
		return
	end
    if (Settings[id] == nil) then
-- No entry for this scenario, creating... shouldn't happen, since addWin should be called first and done this already
		deb("No entry for this scenario, creating...")
		Settings[id] = {}
		ScenarioStats.CheckSettings(id, Settings)
	end
--check for highest damage
	ScenarioStats.checkHigher(Settings, DataSet, id, "HighestDmg", "Damage")
	ScenarioStats.checkHigher(Settings, DataSet, id, "HighestHeal", "Heal")
	ScenarioStats.checkHigher(Settings, DataSet, id, "HighestKillBlows", "KB")
	ScenarioStats.checkHigher(Settings, DataSet, id, "HighestKills", "Kills")
	ScenarioStats.checkHigher(Settings, DataSet, id, "HighestDeaths", "Deaths")
	ScenarioStats.checkHigher(Settings, DataSet, id, "HighestSoloKills", "SoloKills")
	ScenarioStats.checkHigher(Settings, DataSet, id, "HighestRenown", "Renown")
	ScenarioStats.checkHigher(Settings, DataSet, id, "HighestXP", "XP")
-- calculate new averages
	ScenarioStats.addAverage(Settings, DataSet, id, "AvgDmg", "Damage")
	ScenarioStats.addAverage(Settings, DataSet, id, "AvgHeal", "Heal")
	ScenarioStats.addAverage(Settings, DataSet, id, "AvgKillBlows", "KB")
	ScenarioStats.addAverage(Settings, DataSet, id, "AvgKills", "Kills")
	ScenarioStats.addAverage(Settings, DataSet, id, "AvgDeaths", "Deaths")
	ScenarioStats.addAverage(Settings, DataSet, id, "AvgSoloKills", "SoloKills")
	ScenarioStats.addAverage(Settings, DataSet, id, "AvgRenown", "Renown")
	ScenarioStats.addAverage(Settings, DataSet, id, "AvgXP", "XP")
-- increase games counter	
	Settings.TotalGames = Settings.TotalGames + 1
	Settings[id].TotalGames = Settings.TotalGames + 1
end


function ScenarioStats.addWin(side, id, amount)
-- Internal method. Please ignore!
	if not id or not amount or not side then
		d("Critical Error: addWin function called with incorrect Parameters")
		return
	end
    if (ScenarioStats.thisCharacterData[id] == nil) then
-- No entry for this scenario, creating
		deb("No entry for this scenario, creating...")
		ScenarioStats.thisCharacterData[id] = {}
		ScenarioStats.CheckSettings(id, ScenarioStats.thisCharacterData)
		ScenarioStats.CreateDropdownList()
	end
	if (side == "order") then
		ScenarioStats.thisCharacterData[id].Order = ScenarioStats.thisCharacterData[id].Order + amount
		deb(L"Order in"..id..L" increased by "..amount)
		ScenarioStats.thisCharacterData.Order = ScenarioStats.thisCharacterData.Order + amount
		deb(L"Order in total increased by "..amount)
	end
	if (side == "dest") then
		ScenarioStats.thisCharacterData[id].Destruction = ScenarioStats.thisCharacterData[id].Destruction + amount
		deb(L"Destruction in"..id..L" increased by "..amount)
		ScenarioStats.thisCharacterData.Destruction = ScenarioStats.thisCharacterData.Destruction + amount
		deb(L"Destruction in total increased by "..amount)
	end
	if (side == "tie") then
		ScenarioStats.thisCharacterData[id].Tie = ScenarioStats.thisCharacterData[id].Tie + amount
		deb(L"Tie in"..id..L" increased by "..amount)
		ScenarioStats.thisCharacterData.Tie = ScenarioStats.thisCharacterData.Tie + amount
		deb(L"Tie in total increased by "..amount)
	end
end

function ScenarioStats.setWin(side, id, amount)
-- Internal debug method. Please ignore!
	if (DEBUG) then
    if (ScenarioStats.thisCharacterData[id] == nil) then
-- No entry for this scenario, creating
		deb("No entry for this scenario, creating...")
		ScenarioStats.thisCharacterData[id] = {}
		ScenarioStats.thisCharacterData[id].Order = 0
		ScenarioStats.thisCharacterData[id].Destruction = 0
		ScenarioStats.thisCharacterData[id].Tie = 0
	end
	if (side == "order") then
		ScenarioStats.thisCharacterData[id].Order = amount
		deb(L"Order set to "..amount)
	end
	if (side == "dest") then
		ScenarioStats.thisCharacterData[id].Destruction = amount
		deb(L"Destruction set to "..amount)
	end
	if (side == "tie") then
		ScenarioStats.thisCharacterData[id].Tie = amount
		deb(L"Tie set to "..amount)
	end
	end
end

function ScenarioStats.PrintScenarioNames()
	Print(L"List of all recognized Scenario Names")
	Print(L"------------------")
	for id,name in pairs(ScenarioStats.ScenarioNames) do
		Print(towstring(name))
	end
	Print(L"------------------")
	Print(L"If any names that should be there aren't, please inform the author")
end

function ScenarioStats.GetScenName(id)
	local name = GetScenarioName(id)
	if (WStringsCompare(name , L"") ) ~= 0 then
		return name
	else
		return nil
	end
end


-- Convert Functions for Version 0.3a to 1.0
function ScenarioStats.Warning03()
	local windowname ="ScenarioStatsWarningWindow1"
	CreateWindowFromTemplate(windowname, "ScenarioStatsWarningTemplate", "Root")
	text=L"This version of Scenario Statistics is no longer compatible with your saved data.\n\n\nFrom now on data will be saved on a character basis, but it is impossible for this addon to know which characters contributed how much to the saved scores.\n\n However not all is lost. \n\nYou can now either convert the complete data to \"This Character\" (all other characters will then start with a clean statistic) - recommended if this character contributed all or most of the current stats\n\nOr you can \"Reset ALL Data\", this will completely wipe everything (useful when more than one character contributed,and it is impossible to say who contributed what) \n\n Or you choose \"Another Character\", in this case the addon will now shutdown and not record any more data or react to any slash commands, till you choose one of the other two options. Then log into your other character on which you want to keep the data, and you will be shown this window again"
	LabelSetText(windowname.."Text", text)
	LabelSetText(windowname.."TitleBarText", L"Scenario Statstics Warning!")
	CreateWindowFromTemplate(windowname.."Button1", "ScenarioStatsButtonTemplate", "ScenarioStatsWarningWindow1")
	CreateWindowFromTemplate(windowname.."Button2", "ScenarioStatsButtonTemplate", "ScenarioStatsWarningWindow1")
	CreateWindowFromTemplate(windowname.."Button3", "ScenarioStatsButtonTemplate", "ScenarioStatsWarningWindow1")
	WindowClearAnchors(windowname.."Button1")
	WindowAddAnchor(windowname.."Button1", "bottomleft", windowname, "bottomleft", 50, -10)
	WindowAddAnchor(windowname.."Button2", "bottom", windowname, "bottom", 0, -10)
	WindowAddAnchor(windowname.."Button3", "bottomright", windowname, "bottomright", -50, -10)
	WindowSetDimensions(windowname.."Button3", 160, 39)
	WindowSetDimensions(windowname.."Button2", 300, 39)
	WindowSetDimensions(windowname.."Button1", 160, 39)
	ButtonSetText(windowname.."Button1", L"This Character")
	ButtonSetText(windowname.."Button2", L"Another Character (do nothing)")
	ButtonSetText(windowname.."Button3", L"Reset ALL data")
end

function ScenarioStats.WarningWindowButton()
	local windowname ="ScenarioStatsWarningWindow1"
    local mouseWindowname = SystemData.MouseOverWindow.name
	local buttonnumber = tonumber(mouseWindowname:match("^"..windowname.."Button([0-9]+).*"))
	if buttonnumber == 1 then
		deb("Converting to this Character")
		ScenarioStats.Convert03To04()
		DestroyWindow("ScenarioStatsWarningWindow1")
		ScenarioStats.CheckSettingsInit()
	elseif buttonnumber == 2 then
		deb("Doing nothing and shutting down")
		UnregisterEventHandler("Root", SystemData.Events.SCENARIO_END, "ScenarioStats.UpdateStats")
		UnregisterEventHandler("Root", SystemData.Events.SCENARIO_BEGIN, "ScenarioStats.ScenarioStarts")
		UnregisterEventHandler("Root", SystemData.Events.SCENARIO_UPDATE_POINTS, "ScenarioStats.UpdatePoints")
		LibSlash.UnregisterSlashCmd("sstats")
		Print(L"Scenario Statistics successfully deactivated. Please log into another character to convert existing Data")
		DestroyWindow("ScenarioStatsWarningWindow1")
	elseif buttonnumber == 3 then
		deb("Resetting all data")
		DestroyWindow("ScenarioStatsWarningWindow1")
		Reset(L"AlphaDeltaWipe")
	end
end

function ScenarioStats.Convert03To04()
	local newData = {}
	local server = WStringToString(SystemData.Server.Name)
	local character = WStringToString(GameData.Player.name)
	Print("Converting all Data to "..character.." on Server "..server)
	deb("Converting to Server: "..server.." Name: "..character)
	newData[server] = {}
	newData[server][character] = {}
	newData.version = {
						highNumber = 0,
						minorNumber = 4,
					}
	newData[server][character] = deepcopy(ScenarioStats.Data)
	ScenarioStats.Data = deepcopy(newData)
	Print("Convertion complete.")
end


function ScenarioStats.SetOptions(argument)
	lower_argument = string.lower(argument)
	local opt = StringSplit(lower_argument)
	if opt[2] == "on" then 
		on=true
		text = L"Displaying "
	elseif opt[2] == "off" then 
		on=false
		text = L"Not displaying "
	else 
		deb("Wrong argument, displaying help")
		ScenarioStats.HelpSlash()
		return
	end
	if (opt[1] == "damage") then ScenarioStats.Config.Damage = on Print(text..L"damage") end
	if (opt[1] == "heal") then ScenarioStats.Config.Heal = on Print(text..L"heals") end
	if (opt[1] == "kb") then ScenarioStats.Config["Killing Blows"] = on Print(text..L"killing blows") end
	if (opt[1] == "sk") then ScenarioStats.Config["Solo Kills"] = on Print(text..L"solo kills") end
	if (opt[1] == "kills") then ScenarioStats.Config.Kills = on Print(text..L"kills") end
	if (opt[1] == "renown") then ScenarioStats.Config.Renown = on Print(text..L"renown") end
	if (opt[1] == "xp") then ScenarioStats.Config.XP = on Print(text..L"experience") end
	if (opt[1] == "deaths") then ScenarioStats.Config.Deaths = on Print(text..L"deaths") end
end

function ScenarioStats.HelpSlash()
-- show help on invalid slash argument
	Print(L"Scenario Statistics help:")
	Print(L"-------------------------")
	Print(L"/sstats [show] - shows the GUI window")
	Print(L"Rightclick the window to configurate")
end

function ScenarioStats.Slash(input)
	local takeApart = StringSplit(input)
	local argument = ""
	local del = "" -- no delimiter the first time, to prefent space at the beginning
	for index,value in ipairs(takeApart) do 
		if index ~= 1 then
			argument = argument..del..value
			del = " " -- delimiter set to space now
		end
	end
--	if (takeApart[1] == "show") then ScenarioStats.PrintStatsByName(argument)
	if (takeApart[1] == "show") or (takeApart[1] == "") then ScenarioStats.ShowWindowByName("")
--	elseif (takeApart[1] == "showByName") then ScenarioStats.ShowWindowByName(argument) -- for internal testing use only
--	elseif (takeApart[1] == "showNames") then ScenarioStats.PrintScenarioNames()
--	elseif (takeApart[1] == "showByNumber") then ScenarioStats.PrintStats(tonumber(argument)) -- for internal testing use only
--        elseif (takeApart[1] == "set") then ScenarioStats.SetOptions(argument)
--	elseif (takeApart[1] == "resetAll") then ScenarioStats.Reset()
	else ScenarioStats.HelpSlash()
	end
end
