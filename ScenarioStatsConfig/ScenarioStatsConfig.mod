<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="ScenarioStatsConfig" version="1.0" date="2008">
	<Author name="Jarika" email="jarika@gmx.de" />
	<Description text="Config mod for Scenario Statstics - don't disable or it won't save anything" />
        <VersionSettings gameVersion="1.3.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="ScenarioStats" />
		</Dependencies>
		<SavedVariables>
		  	<SavedVariable name="ScenarioStats.Data" />
		  	<SavedVariable name="ScenarioStats.Config" />
		</SavedVariables>
		
        <OnInitialize>
             <CallFunction name="ScenarioStats.Initialize" />
        </OnInitialize>

    </UiMod>
</ModuleFile>
